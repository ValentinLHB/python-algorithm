from functions import send_public_request, send_signed_request
from pprint import pprint


#==========# USER_DATA #==========#


# # Get account info
# account = send_signed_request(
#     'GET',
#     '/api/v3/account'
# )
# pprint(account)


#==========# MARKET_DATA ENDPOINTS #==========#


# # Ping server: https://binance-docs.github.io/apidocs/spot/en/#test-connectivity
# ping = send_public_request(
#     '/api/v3/ping'
# )
# pprint(ping)

# # Check server time: https://binance-docs.github.io/apidocs/spot/en/#check-server-time
# time = send_public_request(
#     '/api/v3/time'
# )
# pprint(time)

# # Get exchange info: https://binance-docs.github.io/apidocs/spot/en/#exchange-information
# exchange_info = send_public_request(
#     '/api/v3/exchangeInfo'
# )
# pprint(exchange_info)

# # Get depth (order book): https://binance-docs.github.io/apidocs/spot/en/#order-book
# depth = send_public_request(
#     '/api/v3/depth',
#     {
#         'symbol': 'BTCUSDT', # Mandatory param
#         'limit': 100 # Optional [5, 10, 20, 50, 100, 500, 1000, 5000]
#     }
# )
# pprint(depth)

# # Get trades: https://binance-docs.github.io/apidocs/spot/en/#recent-trades-list
# trades = send_public_request(
#     '/api/v3/trades',
#     {
#         'symbol' : 'BTCUSDT', # Mandatory
#         'limit' : 500 # Optional [500, 1000]
#     }
# )
# pprint(trades)

# # Get historical trades: https://binance-docs.github.io/apidocs/spot/en/#old-trade-lookup
# historical_trades = send_public_request(
#     '/api/v3/historicalTrades',
#     {
#         'symbol' : 'BTCUSDT', # Mandatory
#         'limit' : 500, # Optional [500, 1000]
#         # 'fromId' : Optional
#     }
# )
# pprint(historical_trades)

# # Get compressed aggregate trades: https://binance-docs.github.io/apidocs/spot/en/#compressed-aggregate-trades-list
# agg_trades = send_public_request(
#     '/api/v3/aggTrades',
#     {
#         'symbol' : 'BTCUSDT', # Mandatory
#         # 'fromId' : '', # Optional
#         # 'startTime' : '', # Optional, timestamp
#         # 'endTime' : '', # Optional, timestamp
#         'limit' : 500 # Optional, [500, 1000]
#     }
# )

# # Get klines: https://binance-docs.github.io/apidocs/spot/en/#kline-candlestick-data
# klines = send_public_request(
#     '/api/v1/klines',
#     {
#         'symbol' : 'BTCUSDT', # Mandatory
#         'interval' : '1d', # Mandatory [1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 8h, 12h, 1d, 3d, 1w, 1M]
#         # 'startTime' : '', # Optional, timestamp
#         # 'endTime' : '', # Optional, timestamp
#         'limit' : 500 # Optional, [500, 1000]
#     }
# )
# pprint(klines)

# # Get current avgPrice: https://binance-docs.github.io/apidocs/spot/en/#current-average-price
# avg_price = send_public_request(
#     '/api/v3/avgPrice',
#     {
#         'symbol' : 'BTCUSDT' # Mandatory
#     }
# )
# pprint(avg_price)

# # Get 24hr price change statistics: https://binance-docs.github.io/apidocs/spot/en/#24hr-ticker-price-change-statistics
# price_chg_24hr = send_public_request(
#     '/api/v3/ticker/24hr',
#     {
#         'symbol' : 'BTCUSDT' # Optional
#     }
# )
# pprint(price_chg_24hr)

# # Get latest price: https://binance-docs.github.io/apidocs/spot/en/#symbol-price-ticker
# price = send_public_request(
#     '/api/v3/ticker/price',
#     {
#         'symbol' : 'BTCUSDT' # Optional
#     }
# )
# print(price)

# # Get symbol order book: https://binance-docs.github.io/apidocs/spot/en/#symbol-order-book-ticker
# book_ticker = send_public_request(
#     '/api/v3/ticker/bookTicker',
#     {
#         'symbol' : 'BTCUSDT' # Optional
#     }
# )
# pprint(book_ticker)
