# Imports
from ta.trend import vortex_indicator_neg, vortex_indicator_pos
import requests, json
from pprint import pprint
import pandas as pd

# Variables
SYMBOL = "BTCUSDT"
INTERVAL = "1d"
BASE_URL = "https://fapi.binance.com"
WINDOW = 14
POSITION = 0
pnls = []

# Request historical data
klines = requests.get(
    BASE_URL + "/fapi/v1/klines",
    params = {
        'symbol': SYMBOL,
        'interval': INTERVAL
    }
).json()
# pprint(klines)

# Technical indicator: Vortex Indicator
df = pd.DataFrame(
    data = klines,
    columns = [
        'open_time',
        'open',
        'high',
        'low',
        'close',
        'volume',
        'close_time',
        'quote_asset_volume',
        'number_of_trades',
        'taker_buy_base_asset_vol',
        'taker_buy_quote_asset_vol',
        'Ignore'
    ]
)

df['high'] = df['high'].astype(float)
df['low'] = df['low'].astype(float)
df['close'] = df['close'].astype(float)

vi_pos = vortex_indicator_pos(
    high=df['high'],
    low=df['low'],
    close=df['close'],
    window=WINDOW,
    fillna=False
)
vi_neg = vortex_indicator_neg(
    high=df['high'],
    low=df['low'],
    close=df['close'],
    window=WINDOW
)

if len(vi_neg) == len(vi_pos):

    for n in range(len(vi_pos)):
        
        print(
            '\n'
            '\nVI+ :', vi_pos[n],
            '\nVI- :', vi_neg[n]
        )

        if POSITION == 0:

            if vi_pos[n] > vi_neg[n]:
                POSITION = 1
                open_long = df['close'][n]
                print(
                    'OPEN LONG: {}'.format(
                        open_long
                    )
                )

            elif vi_pos[n] <= vi_neg[n]:
                POSITION = -1
                open_short = df['close'][n]
                print(
                    'OPEN SHORT: {}'.format(
                        open_short
                    )
                )
            
            else:
                pass

        elif POSITION == 1:

            if vi_pos[n] <= vi_neg[n]:
                POSITION = 0
                close_long = df['close'][n]
                pnls.append(close_long - open_long)
                print(
                    'CLOSE LONG: {}'.format(
                        close_long
                    )
                )
                print(
                    'PNL: {}'.format(
                        close_long - open_long
                    )
                )

            else:
                pass

        elif POSITION == -1:

            if vi_pos[n] > vi_neg[n]:
                POSITION = 0
                close_short = df['close'][n]
                pnls.append(open_short - close_short)
                print(
                    'CLOSE SHORT: {}'.format(
                        close_short
                    )
                )
                print(
                    'PNL: {}'.format(
                        open_short - close_short
                    )
                )
        
        else:
            pass

# Backtesting results
print(
    'Total PNL: $ {}'.format(
        round(sum(pnls), 2)
    )
)
