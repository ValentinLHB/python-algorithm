portfolio = [
    {
        'pair' : 'BTCUSDT',
        'interval' : '1d',
        'weight' : 0.75
    },
    {
        'pair' : 'ETHUSDT',
        'interval' : '1d',
        'weight' : 0.25
    }
]