# Imports
from collections import deque
from functions import futures_get_hist
from portfolio import portfolio
from pprint import pprint
import pandas as pd

from ta.trend import vortex_indicator_pos, vortex_indicator_neg

class Algorithm:

    def __init__(self, symbol, interval, weight):
        self.symbol = symbol
        self.interval = interval
        self.weight = weight
        self.high = deque([], maxlen=500)
        self.low = deque([], maxlen=500)
        self.close = deque([], maxlen=500)
        self.tmp_high = deque([], maxlen=500)
        self.tmp_low = deque([], maxlen=500)
        self.tmp_close = deque([], maxlen=500)
        self.position = 0

        historical_data = futures_get_hist(
            symbol = symbol,
            interval = interval
        )
        pprint(historical_data)

        for candle in historical_data:
            self.high.append(float(candle[2]))
            self.low.append(float(candle[3]))
            self.close.append(float(candle[4]))

        # When candle is open, append all data from high, low, close to 
        # tmp_high, tmp_low, tmp_close and append candle['h'], candle['l'], 
        # candle['c'] to respective tmp lists

# Variables

# Get historical data for backtest


# Technical indicator: Vortex Indicator (VI)
# Source: https://www.vortexfund.com/the-vortex-indicator

# Websocket functions (live implementation)


# WebSocketApp (live implementation)
