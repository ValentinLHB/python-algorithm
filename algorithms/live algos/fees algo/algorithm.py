# Imports
from collections import deque
from portfolio import portfolio
from functions import futures_get_hist
from pprint import pprint
import pandas as pd

# Implement technical analysis
from ta.utils import dropna
from ta.trend import EMAIndicator

class Algorithm:

    def __init__(self, symbol, interval):
        self.symbol = symbol
        self.interval = interval
        self.data = deque([], maxlen=500)
        self.tmp = deque([], maxlen=500)
        self.position = 0

        historical_data = futures_get_hist(
            symbol = symbol,
            interval = interval
        )

        for candle in historical_data:
            self.data.append(float(candle[4]))
    
    def get_ticks(self, candle):

        start_cash = 100 # 100$
        account = []

        if account == []:
            account.append(start_cash)

        account_size_btcusdt = account / float(candle['c']) # % of btcusdt i can buy with my account
        account_risk = 0.01 # Account risk is 1%
        invalidation_point = 0.1 # If price goes up/down by 10%, position is exited
        position_size = float(sum(account)) * account_risk / invalidation_point # You can't win or lose more than this on one trade
        fee_rate = 0.04 / 100 # maker: 0.02% / taker: 0.04%
        fee = position_size * fee_rate
        fees_list = []

        self.open_long = []
        self.open_short = []
        self.close_long = []
        self.close_short = []

        # When candle is open, append all data from self.data in tmp list and append tick data candle['c'] to tmp
        if candle['x'] == False:
            for close in list(self.data):
                self.tmp.append(close)
            self.tmp.append(float(candle['c']))

            df = pd.DataFrame(
                data = list(self.tmp),
                columns = ['close']
            )

            emas_Used = [3, 5, 8, 10, 12, 15, 30, 35, 40, 45, 50, 60]

            for x in emas_Used:
                ema = x
                indicator_ema = EMAIndicator(
                    close=df['close'],
                    window=x,
                    fillna=False
                )
                df['ema_' + str(ema)] = indicator_ema.ema_indicator()

            for i in df.index:
                c_min = min(
                    df['ema_3'][i], 
                    df['ema_5'][i], 
                    df['ema_8'][i], 
                    df['ema_10'][i], 
                    df['ema_12'][i],
                    df['ema_15'][i],
                )
                c_max = max(
                    df['ema_30'][i], 
                    df['ema_35'][i], 
                    df['ema_40'][i], 
                    df['ema_45'][i], 
                    df['ema_50'][i],
                    df['ema_60'][i],
                )

            # Positions
            if self.position == 0:

                # Entry point when short term emas > long term emas
                if c_min > c_max:
                    print('GO LONG')
                    self.position = 1
                    self.open_long.append(float(df['close'].iloc[-1]))                    

                # Short condition is met
                elif c_max > c_min:
                    print('GO SHORT')
                    self.position = -1
                    self.open_short.append(float(df['close'].iloc[-1]))

                else:
                    pass

            elif self.position == 1:

                # Invalidation point is met: exit position
                if c_max > c_min or abs((self.open_long[-1] - float(df['close'].iloc[-1])) / self.open_long[-1]) > invalidation_point:
                    print('CLOSE LONG')
                    self.position = 0
                    self.close_long.append(float(df['close'].iloc[-1]))
                    account.append((self.close_long[-1] / self.open_long[-1] - 1) * position_size)

                else:
                    pass

            elif self.position == -1:

                # Invalidation point is met: exit position
                if c_min > c_max or abs((self.open_short[-1] - float(df['close'].iloc[-1])) / self.open_short[-1]) > invalidation_point:
                    print('CLOSE SHORT')
                    self.position = 0
                    self.close_short.append(float(df['close'].iloc[-1]))
                    account.append((self.open_short[-1] / self.close_short[-1] - 1) * position_size)

                else:
                    pass

            else:

                print('Error')
        
        # When candle is closed, append close price to data
        else:
            self.data.append(float(candle['c']))

        # print(
        #     self.symbol, '| c_min: {} | c_max: {} | pos: {}\n'.format(
        #         round(c_min, 2),
        #         round(c_max, 2),
        #         self.position
        #     )
        # )

        print('Account: $', sum(account))
