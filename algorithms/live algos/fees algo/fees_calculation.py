import json, requests, websocket
import numpy as np
from pprint import pprint
import requests, json

# Account & risk management
btcusdt_price = 40000
account_size = 100 # $
account_size_btcusdt = account_size / btcusdt_price
account_risk = 0.01
invalidation_point = 0.1 # +/-10% distance to stop-loss
position_size = account_size * account_risk / invalidation_point # You can't lose more than the position_size on a single trade

# Fee calculation
fee_rate = 0.04 / 100 # maker: 0.02% / taker: 0.04%
fees = position_size * fee_rate

# print(
#     'Position size: ${}'.format(position_size)
# )

# print(
#     'You own {} BTC (${})'.format(
#         account_size_btcusdt,
#         account_size
#     )
# )

# print('$', fees)
